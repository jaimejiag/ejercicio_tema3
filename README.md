### Ejercicio 1 ###
En el activity aparecen unos campos para rellenar los datos de un empleado, se añaden en un ArrayList cuando se pulsa en el botón correspondiente.
Se mostrarán los empleados añadidos al pulsar el botón "Mostrar".
El archivo XML se crea con los datos de los empleados añadidos en la lista, se guardará en la memoria externa en un fichero llamado empleados.txt.

### Ejercicio 2 ###
Muestra los datos meteorológicos facilitados por AEMET.
Para la correcta visualización de los datos comprobar que el dispositivo donde se ejecute la app tenga la hora y fecha actualizada.

### Ejercicio 3 ###
Muestra una lista con las estaciones de bicicletas de la ciudad de Zaragoza.
Al pulsar en alguna de las estaciones se mostrará datos sobre esa estación, al pulsar en "Ver mapa" se abrirá Google Maps apuntando en la ciudad de Zaragoza. El documento xml que facilita la web no tiene las coordenadas de las estaciones correctamente redactadas, por eso decidí que se mostrara la ciudad.

### Ejercicio 4 ###
Hay tres botones; uno muestra una lista de noticias de PCWorld, otro muestra una lista con noticias de Linux Magazine y el último muestra noticias de Andalucía en Europapress. Al pulsar en alguna de las noticias se mostrará al completo en la web.