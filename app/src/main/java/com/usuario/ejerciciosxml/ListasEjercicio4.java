package com.usuario.ejerciciosxml;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.loopj.android.http.FileAsyncHttpResponseHandler;
import com.usuario.ejerciciosxml.modelo.EstacionBici;
import com.usuario.ejerciciosxml.modelo.Rss;

import org.xmlpull.v1.XmlPullParserException;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;

import static com.usuario.ejerciciosxml.Ejercicio4Activity.KEY_RSS;

/**
 * Created by jaime on 7/12/16.
 */

public class ListasEjercicio4 extends AppCompatActivity {
    private static final String TEMPORAL = "rss.xml";
    private ListView mLstvLista;
    private ViewGroup mLayout;

    private ArrayList<Rss> rssList;
    private ArrayAdapter<Rss> adapter;
    private String url;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ejercicio3);

        url = getIntent().getExtras().getString(KEY_RSS);
        mLayout = (CoordinatorLayout) findViewById(R.id.activity_ejercicio3);

        mLstvLista = (ListView) findViewById(R.id.lstv_ej3_lista);
        descarga();
        mLstvLista.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Uri uri = Uri.parse((String) rssList.get(i).getLink());
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                if (intent.resolveActivity(getPackageManager()) != null)
                    startActivity(intent);
                else
                    Snackbar.make(mLayout, "No hay un navegador", Snackbar.LENGTH_SHORT).show();

            }
        });
    }

    private void descarga() {
        final ProgressDialog progreso = new ProgressDialog(this);
        File miFichero = new File(Environment.getExternalStorageDirectory().getAbsolutePath(), TEMPORAL);

        RestClient.get(url, new FileAsyncHttpResponseHandler(miFichero) {
            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, File file) {
                progreso.dismiss();
                Snackbar.make(mLayout, "Fallo: " + throwable.getMessage(), Snackbar.LENGTH_SHORT).show();
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, File file) {
                progreso.dismiss();

                try {
                    rssList = Analisis.analizarRSS(file);
                    mostrar();
                } catch (XmlPullParserException e) {
                    Snackbar.make(mLayout, "Excepción XML: " + e.getMessage(), Snackbar.LENGTH_SHORT).show();
                } catch (IOException e) {
                    Snackbar.make(mLayout, "Excepción I/O: " + e.getMessage(), Snackbar.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onStart() {
                super.onStart();
                progreso.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                progreso.setMessage("Conectando . . .");
                progreso.setCancelable(false);
                progreso.show();
            }
        });
    }

    private void mostrar() {
        if (rssList != null) {
            if (adapter == null) {
                adapter = new ArrayAdapter<Rss>(this, android.R.layout.simple_list_item_1, rssList);
                mLstvLista.setAdapter(adapter);
            } else {
                adapter.clear();
                adapter.addAll(rssList);
            }
        } else
            Snackbar.make(mLayout, "Error al crear la lista", Snackbar.LENGTH_SHORT).show();
    }
}
