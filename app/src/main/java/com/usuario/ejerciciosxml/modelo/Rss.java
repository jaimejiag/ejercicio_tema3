package com.usuario.ejerciciosxml.modelo;

/**
 * Created by jaime on 7/12/16.
 */

public class Rss {
    private String titulo;
    private String link;

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    @Override
    public String toString() {
        return titulo;
    }
}
