package com.usuario.ejerciciosxml.modelo;

/**
 * Created by jaime on 6/12/16.
 */

public class EstacionBici {
    private String titulo;
    private String estado;
    private String disponibles;
    private String anclajes;
    private String modificacion;
    private String coordinadas;

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getDisponibles() {
        return disponibles;
    }

    public void setDisponibles(String disponibles) {
        this.disponibles = disponibles;
    }

    public String getAnclajes() {
        return anclajes;
    }

    public void setAnclajes(String anclajes) {
        this.anclajes = anclajes;
    }

    public String getModificacion() {
        return modificacion;
    }

    public void setModificacion(String modificacion) {
        this.modificacion = modificacion;
    }

    public String getCoordinadas() {
        return coordinadas;
    }

    public void setCoordinadas(String coordinadas) {
        this.coordinadas = coordinadas;
    }

    @Override
    public String toString() {
        return titulo;
    }
}
