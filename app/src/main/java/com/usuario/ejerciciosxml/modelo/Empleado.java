package com.usuario.ejerciciosxml.modelo;

/**
 * Created by usuario on 1/12/16.
 */

public class Empleado {
    private String mNombre;
    private String mPuesto;
    private int mEdad;
    private double mSueldo;

    public Empleado () {
        mNombre = "";
        mPuesto = "";
        mEdad = 0;
        mSueldo = 0.0;
    }

    public Empleado(String nombre, String puesto, int edad, double sueldo) {
        mNombre = nombre;
        mPuesto = puesto;
        mEdad = edad;
        mSueldo = sueldo;
    }

    public String getmNombre() {
        return mNombre;
    }

    public void setmNombre(String mNombre) {
        this.mNombre = mNombre;
    }

    public String getmPuesto() {
        return mPuesto;
    }

    public void setmPuesto(String mPuesto) {
        this.mPuesto = mPuesto;
    }

    public int getmEdad() {
        return mEdad;
    }

    public void setmEdad(int mEdad) {
        this.mEdad = mEdad;
    }

    public double getmSueldo() {
        return mSueldo;
    }

    public void setmSueldo(double mSueldo) {
        this.mSueldo = mSueldo;
    }

    @Override
    public String toString() {
        return super.toString();
    }
}
