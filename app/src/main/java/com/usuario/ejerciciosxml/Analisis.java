package com.usuario.ejerciciosxml;

import android.os.Environment;
import android.util.Xml;

import com.usuario.ejerciciosxml.modelo.Empleado;
import com.usuario.ejerciciosxml.modelo.EstacionBici;
import com.usuario.ejerciciosxml.modelo.Rss;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlSerializer;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * Created by usuario on 2/12/16.
 */

public class Analisis {

    public static void crearXML(ArrayList<Empleado> empleados, String fichero) throws IOException {
        FileOutputStream fout;
        fout = new FileOutputStream(new File(Environment.getExternalStorageDirectory().getAbsolutePath(), fichero));
        XmlSerializer serializer = Xml.newSerializer();

        serializer.setOutput(fout, "UTF-8");
        serializer.startDocument(null, true);
        serializer.setFeature("http://xmlpull.org/v1/doc/features.html#indent-output", true);
        serializer.startTag(null, "empresa");

        for (int i = 0; i < empleados.size(); i++) {
            serializer.startTag(null, "empleado");
            serializer.startTag(null, "nombre");
            serializer.text(empleados.get(i).getmNombre());
            serializer.endTag(null, "nombre");
            serializer.startTag(null, "puesto");
            serializer.text(empleados.get(i).getmPuesto());
            serializer.endTag(null, "puesto");
            serializer.startTag(null, "edad");
            serializer.text(String.valueOf(empleados.get(i).getmEdad()));
            serializer.endTag(null, "edad");
            serializer.startTag(null, "sueldo");
            serializer.text(String.valueOf(empleados.get(i).getmSueldo()));
            serializer.endTag(null, "sueldo");
            serializer.endTag(null, "empleado");
        }

        serializer.endTag(null, "empresa");
        serializer.endDocument();
        serializer.flush();
        fout.close();
    }

    public static ArrayList<String> analizarXML(File file) throws NullPointerException, XmlPullParserException, IOException {
        boolean dentroDia = false;
        boolean dentroTemperatura = false;
        int contador = 0;
        StringBuilder builder = new StringBuilder();
        XmlPullParser xpp = Xml.newPullParser();
        ArrayList<String> resultados = new ArrayList<>();
        Calendar cal = Calendar.getInstance(Locale.getDefault());

        xpp.setInput(new FileReader(file));
        int eventType = xpp.getEventType();

        while (eventType != XmlPullParser.END_DOCUMENT) {
            switch (eventType) {
                case XmlPullParser.START_TAG:
                    if (xpp.getName().equalsIgnoreCase("dia") && contador < 2)
                        dentroDia = true;
                    else if (xpp.getName().equalsIgnoreCase("estado_cielo") && dentroDia) {
                        if (contador < 1) {
                            if (xpp.getAttributeValue(0).equalsIgnoreCase("00-06") && (new Date().getHours() < 6))
                                builder.append(xpp.getAttributeValue(0) + ": \t" + xpp.getAttributeValue(1) + "\n");
                            if ((xpp.getAttributeValue(0).equalsIgnoreCase("06-12") && (new Date().getHours() < 12)))
                                builder.append(xpp.getAttributeValue(0)+ ": \t" + xpp.getAttributeValue(1) + "\n");
                            if ((xpp.getAttributeValue(0).equalsIgnoreCase("12-18") && (new Date().getHours() < 18)))
                                builder.append(xpp.getAttributeValue(0)+ ": \t" + xpp.getAttributeValue(1) + "\n");
                            if (xpp.getAttributeValue(0).equalsIgnoreCase("18-24"))
                                builder.append(xpp.getAttributeValue(0)+ ": \t" + xpp.getAttributeValue(1) + "\n");
                        } else {
                            if (xpp.getAttributeValue(0).equalsIgnoreCase("00-06"))
                                builder.append(xpp.getAttributeValue(0) + ": \t" + xpp.getAttributeValue(1) + "\n");
                            if ((xpp.getAttributeValue(0).equalsIgnoreCase("06-12")))
                                builder.append(xpp.getAttributeValue(0)+ ": \t" + xpp.getAttributeValue(1) + "\n");
                            if ((xpp.getAttributeValue(0).equalsIgnoreCase("12-18")))
                                builder.append(xpp.getAttributeValue(0)+ ": \t" + xpp.getAttributeValue(1) + "\n");
                            if (xpp.getAttributeValue(0).equalsIgnoreCase("18-24"))
                                builder.append(xpp.getAttributeValue(0)+ ": \t" + xpp.getAttributeValue(1) + "\n");
                        }
                    } else if (xpp.getName().equalsIgnoreCase("temperatura") && dentroDia)
                        dentroTemperatura = true;
                    else if (xpp.getName().equalsIgnoreCase("maxima") && dentroTemperatura && dentroDia)
                        builder.append("Temp. máx/min (ºC): \t" + xpp.nextText());
                    else if (xpp.getName().equalsIgnoreCase("minima") && dentroTemperatura && dentroDia)
                        builder.append(" / " + xpp.nextText() + "\n");
                    break;

                case XmlPullParser.END_TAG:
                    if (xpp.getName().equalsIgnoreCase("dia")) {
                        dentroDia = false;
                        contador++;
                        resultados.add(builder.toString());
                        builder.delete(0, builder.length() - 1);
                    }

                    if (xpp.getName().equalsIgnoreCase("temperatura"))
                        dentroTemperatura = false;
                    break;
            }

            eventType = xpp.next();
        }

        return resultados;
    }

    public static ArrayList<EstacionBici> analizarEstaciones(File file) throws XmlPullParserException, IOException {
        int eventType;
        ArrayList<EstacionBici> estaciones = null;
        EstacionBici estacion = null;
        boolean dentroEstacion = false;

        XmlPullParser xpp = Xml.newPullParser();
        xpp.setInput(new FileReader(file));
        eventType = xpp.getEventType();

        while (eventType != XmlPullParser.END_DOCUMENT) {
            switch (eventType) {
                case XmlPullParser.START_DOCUMENT:
                    estaciones = new ArrayList<EstacionBici>();
                    break;

                case XmlPullParser.START_TAG:
                    if (xpp.getName().equalsIgnoreCase("estacion")) {
                        dentroEstacion = true;
                        estacion = new EstacionBici();
                    } else if (xpp.getName().equalsIgnoreCase("title") && dentroEstacion)
                        estacion.setTitulo(xpp.nextText());
                    else if (xpp.getName().equalsIgnoreCase("estado") && dentroEstacion)
                        estacion.setEstado(xpp.nextText());
                    else if (xpp.getName().equalsIgnoreCase("bicisDisponibles") && dentroEstacion)
                        estacion.setDisponibles(xpp.nextText());
                    else if (xpp.getName().equalsIgnoreCase("anclajesDisponibles") && dentroEstacion)
                        estacion.setAnclajes(xpp.nextText());
                    else if (xpp.getName().equalsIgnoreCase("lastUpdated") && dentroEstacion)
                        estacion.setModificacion(xpp.nextText());
                    else if (xpp.getName().equalsIgnoreCase("coordinates") && dentroEstacion)
                        estacion.setCoordinadas(xpp.nextText());
                    break;

                case XmlPullParser.END_TAG:
                    if (xpp.getName().equalsIgnoreCase("estacion")) {
                        dentroEstacion = false;
                        estaciones.add(estacion);
                    }
                    break;
            }
            eventType = xpp.next();
        }
        return estaciones;
    }

    public static ArrayList<Rss> analizarRSS(File file) throws XmlPullParserException, IOException {
        int eventType;
        ArrayList<Rss> rssList = null;
        Rss rss = null;
        boolean dentroItem = false;

        XmlPullParser xpp = Xml.newPullParser();
        xpp.setInput(new FileReader(file));
        eventType = xpp.getEventType();

        while (eventType != XmlPullParser.END_DOCUMENT) {
            switch (eventType) {
                case XmlPullParser.START_DOCUMENT:
                    rssList = new ArrayList<Rss>();
                    break;

                case XmlPullParser.START_TAG:
                    if (xpp.getName().equalsIgnoreCase("item")) {
                        dentroItem = true;
                        rss = new Rss();
                    } else if (xpp.getName().equalsIgnoreCase("title") && dentroItem)
                        rss.setTitulo(xpp.nextText());
                    else if (xpp.getName().equalsIgnoreCase("link") && dentroItem)
                        rss.setLink(xpp.nextText());
                    break;

                case XmlPullParser.END_TAG:
                    if (xpp.getName().equalsIgnoreCase("item")) {
                        dentroItem = false;
                        rssList.add(rss);
                    }
                    break;
            }
            eventType = xpp.next();
        }
        return rssList;
    }

}
