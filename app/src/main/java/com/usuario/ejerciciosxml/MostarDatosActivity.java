package com.usuario.ejerciciosxml;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;

public class MostarDatosActivity extends AppCompatActivity {
    TextView mTxvDatos;
    Button mBtnVer;
    ArrayList<String> mDatos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mostar_datos);

        mTxvDatos = (TextView) findViewById(R.id.txv_ej3_datos);
        mDatos = getIntent().getStringArrayListExtra(Ejercicio3Activity.KEY_ESTACIONES);

        mTxvDatos.setText("Estado: " + mDatos.get(0) + "\n" +
                "Bicis disponibles: " + mDatos.get(1) + "\n" +
                "Anclajes disponibles: " + mDatos.get(2) + "\n" +
                "Útima modificación: " + mDatos.get(3) + "\n");

        mBtnVer = (Button) findViewById(R.id.btn_ej3_ver);
        mBtnVer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(android.content.Intent.ACTION_VIEW,
                        Uri.parse("http://maps.google.com/maps?q=41.6563497,-0.876566"));
                startActivity(intent);
            }
        });
    }
}
