package com.usuario.ejerciciosxml;

import android.app.ProgressDialog;
import android.os.Environment;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.loopj.android.http.FileAsyncHttpResponseHandler;

import org.xmlpull.v1.XmlPullParserException;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

import cz.msebera.android.httpclient.Header;

public class Ejercicio2Activity extends AppCompatActivity {
    private static final String URL = "http://www.aemet.es/xml/municipios/localidad_29067.xml";
    private static final String TEMPORAL = "tiempo.xml";

    ViewGroup mLayout;
    TextView mTxvFecha1;
    TextView mTxvFecha2;
    TextView mTxvResultado1;
    TextView mTxvResultado2;

    ArrayList<String> mResultados;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ejercicio2);

        mLayout = (RelativeLayout) findViewById(R.id.activity_ejercicio2);
        mTxvFecha1 = (TextView) findViewById(R.id.txv_ej2_fecha1);
        mTxvFecha2 = (TextView)findViewById(R.id.txv_ej2_fecha2);
        mTxvResultado1 = (TextView)findViewById(R.id.txv_ej2_resultado1);
        mTxvResultado2 = (TextView)findViewById(R.id.txv_ej2_resultado2);

        SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault());
        Calendar cal = Calendar.getInstance(Locale.getDefault());
        mTxvFecha1.setText(df.format(cal.getTime()));
        cal.add(Calendar.DATE, 1);
        mTxvFecha2.setText(df.format(cal.getTime()));

        descarga();
    }

    private void descarga() {
        final ProgressDialog progreso = new ProgressDialog(this);
        final File miFichero = new File(Environment.getExternalStorageDirectory().getAbsolutePath(), TEMPORAL);
        RestClient.get(URL, new FileAsyncHttpResponseHandler(miFichero) {
            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, File file) {
                progreso.dismiss();
                Snackbar.make(mLayout, "Fallo: " + statusCode + "\n" + throwable.getMessage(), Snackbar.LENGTH_SHORT).show();
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, File file) {
                progreso.dismiss();
                try {
                    mResultados = Analisis.analizarXML(miFichero);
                    mTxvResultado1.setText(mResultados.get(0));
                    mTxvResultado2.setText(mResultados.get(1));
                } catch (XmlPullParserException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    Snackbar.make(mLayout, "Fallo al intentar analizar el archivo XML", Snackbar.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onStart() {
                super.onStart();
                progreso.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                progreso.setMessage("Conectando . . .");
                progreso.setCancelable(false);
                progreso.show();
            }
        });
    }
}
