package com.usuario.ejerciciosxml;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Environment;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.loopj.android.http.FileAsyncHttpResponseHandler;
import com.usuario.ejerciciosxml.modelo.EstacionBici;

import org.xmlpull.v1.XmlPullParserException;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;

public class Ejercicio3Activity extends AppCompatActivity {
    private static final String URL = "http://www.zaragoza.es/api/recurso/urbanismo-infraestructuras/estacion-bicicleta.xml";
    private static final String TEMPORAL = "estaciones.xml";
    public static final String KEY_ESTACIONES = "estaciones";

    ListView mLstvLista;
    ViewGroup mLayout;

    ArrayList<EstacionBici> estaciones;
    ArrayAdapter<EstacionBici> adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ejercicio3);

        mLayout = (CoordinatorLayout) findViewById(R.id.activity_ejercicio3);


        mLstvLista = (ListView)findViewById(R.id.lstv_ej3_lista);
        descarga();
        mLstvLista.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent intent = new Intent(getApplicationContext(), MostarDatosActivity.class);
                intent.putStringArrayListExtra(KEY_ESTACIONES, obtenerDatos(i));
                startActivity(intent);
            }
        });
    }

    private void descarga() {
        final ProgressDialog progreso = new ProgressDialog(this);
        File miFichero = new File(Environment.getExternalStorageDirectory().getAbsolutePath(), TEMPORAL);

        RestClient.get(URL, new FileAsyncHttpResponseHandler(miFichero) {
            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, File file) {
                progreso.dismiss();
                Snackbar.make(mLayout, "Fallo: " + throwable.getMessage(), Snackbar.LENGTH_SHORT).show();
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, File file) {
                progreso.dismiss();

                try {
                    estaciones = Analisis.analizarEstaciones(file);
                    mostrar();
                } catch (XmlPullParserException e) {
                    Snackbar.make(mLayout, "Excepción XML: " + e.getMessage(), Snackbar.LENGTH_SHORT).show();
                } catch (IOException e) {
                    Snackbar.make(mLayout, "Excepción I/O: " + e.getMessage(), Snackbar.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onStart() {
                super.onStart();
                progreso.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                progreso.setMessage("Conectando . . .");
                progreso.setCancelable(false);
                progreso.show();
            }
        });
    }

    private void mostrar() {
        if (estaciones != null){
            if (adapter == null) {
                adapter = new ArrayAdapter<EstacionBici>(this, android.R.layout.simple_list_item_1, estaciones);
                mLstvLista.setAdapter(adapter);
            } else {
                adapter.clear();
                adapter.addAll(estaciones);
            }
        } else
            Snackbar.make(mLayout, "Error al crear la lista", Snackbar.LENGTH_SHORT).show();
    }

    private ArrayList<String> obtenerDatos(int posicion) {
        ArrayList<String> datos = new ArrayList<>();

        datos.add(estaciones.get(posicion).getEstado());
        datos.add(estaciones.get(posicion).getDisponibles());
        datos.add(estaciones.get(posicion).getAnclajes());
        datos.add(estaciones.get(posicion).getModificacion());
        datos.add(estaciones.get(posicion).getCoordinadas());

        return datos;
    }
}
