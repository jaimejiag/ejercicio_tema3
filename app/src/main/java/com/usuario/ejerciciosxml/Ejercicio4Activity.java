package com.usuario.ejerciciosxml;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class Ejercicio4Activity extends AppCompatActivity {
    private static final String RSS_PC = "http://www.pcworld.com/index.rss";
    private static final String RSS_LINUX = "http://www.linux-magazine.com/rss/feed/lmi_news";
    private static final String RSS_NOTICIAS = "http://www.europapress.es/rss/rss.aspx?ch=279";
    public static final String KEY_RSS = "rss";

    Button mBtnPC;
    Button mBtnLinux;
    Button mBtnNoticias;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ejercicio4);

        mBtnPC = (Button) findViewById(R.id.btn_ej4_pc);
        mBtnPC.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), ListasEjercicio4.class);
                intent.putExtra(KEY_RSS, RSS_PC);
                startActivity(intent);
            }
        });

        mBtnLinux = (Button) findViewById(R.id.btn_ej4_linux);
        mBtnLinux.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), ListasEjercicio4.class);
                intent.putExtra(KEY_RSS, RSS_LINUX);
                startActivity(intent);
            }
        });

        mBtnNoticias = (Button) findViewById(R.id.btn_ej4_noticias);
        mBtnNoticias.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), ListasEjercicio4.class);
                intent.putExtra(KEY_RSS, RSS_NOTICIAS);
                startActivity(intent);
            }
        });
    }
}
