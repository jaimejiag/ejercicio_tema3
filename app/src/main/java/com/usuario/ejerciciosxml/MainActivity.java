package com.usuario.ejerciciosxml;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {
    private Button mBtnEj1;
    private Button mBtnEj2;
    private Button mBtnEj3;
    private Button mBtnEj4;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initialize();
    }

    private void initialize() {
        mBtnEj1 = (Button) findViewById(R.id.btn_ej1);
        mBtnEj1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent;
                intent = new Intent(MainActivity.this, Ejercicio1Activity.class);
                startActivity(intent);
            }
        });

        mBtnEj2 = (Button) findViewById(R.id.btn_ej2);
        mBtnEj2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent;
                intent = new Intent(MainActivity.this, Ejercicio2Activity.class);
                startActivity(intent);
            }
        });

        mBtnEj3 = (Button) findViewById(R.id.btn_ej3);
        mBtnEj3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent;
                intent = new Intent(MainActivity.this, Ejercicio3Activity.class);
                startActivity(intent);
            }
        });

        mBtnEj4 = (Button) findViewById(R.id.btn_ej4);
        mBtnEj4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent;
                intent = new Intent(MainActivity.this, Ejercicio4Activity.class);
                startActivity(intent);
            }
        });
    }
}
