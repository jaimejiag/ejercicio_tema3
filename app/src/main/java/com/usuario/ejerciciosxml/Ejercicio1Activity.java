package com.usuario.ejerciciosxml;

import android.graphics.drawable.Drawable;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.usuario.ejerciciosxml.modelo.Empleado;

import java.io.IOException;
import java.util.ArrayList;

public class Ejercicio1Activity extends AppCompatActivity {
    private static final String NOMBRE_FICHERO = "empleados.txt";

    EditText mEdtNombre;
    EditText mEdtPuesto;
    EditText mEdtEdad;
    EditText mEdtSueldo;
    Button mBtnAnyadir;
    Button mBtnMostrar;
    Button mBtnCrear;
    TextView mTxvDatos;
    ViewGroup mLayout;

    ArrayList<Empleado> mEmpleados;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ejercicio1);

        inicializar();
    }

    private void inicializar() {
        mEdtNombre = (EditText) findViewById(R.id.edt_ej1_nombre);
        mEdtPuesto = (EditText) findViewById(R.id.edt_ej1_puesto);
        mEdtEdad = (EditText) findViewById(R.id.edt_ej1_edad);
        mEdtSueldo = (EditText) findViewById(R.id.edt_ej1_sueldo);
        mTxvDatos = (TextView) findViewById(R.id.txv_ej1_datos);
        mLayout = (RelativeLayout) findViewById(R.id.activity_ejercicio1);

        mEmpleados = new ArrayList<>();

        mBtnAnyadir = (Button) findViewById(R.id.btn_ej1_anyadir);
        mBtnAnyadir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                anyadirEmpleado();
            }
        });

        mBtnMostrar = (Button) findViewById(R.id.btn_ej1_mostrar);
        mBtnMostrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mostrarDatos();
            }
        });

        mBtnCrear = (Button) findViewById(R.id.btn_ej1_crear);
        mBtnCrear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                crearXML();
            }
        });
    }

    private boolean validarCampos() {
        boolean resultado = true;
        String mensaje = "El campo no debe estar vacío";

        if (TextUtils.isEmpty(mEdtNombre.getText().toString())) {
            mEdtNombre.setError(mensaje);
            resultado = false;
        }

        if (TextUtils.isEmpty(mEdtPuesto.getText().toString())) {
            mEdtPuesto.setError(mensaje);
            resultado = false;
        }

        if (TextUtils.isEmpty(mEdtEdad.getText().toString())) {
            mEdtEdad.setError(mensaje);
            resultado = false;
        }

        if (TextUtils.isEmpty(mEdtSueldo.getText().toString())) {
            mEdtSueldo.setError(mensaje);
            resultado = false;
        }

        return  resultado;
    }

    private void vaciarCampos() {
        mEdtNombre.setText("");
        mEdtPuesto.setText("");
        mEdtEdad.setText("");
        mEdtSueldo.setText("");
    }

    private void anyadirEmpleado() {
        Empleado empleado;

        if (validarCampos()) {
            empleado = new Empleado(
                    mEdtNombre.getText().toString(),
                    mEdtPuesto.getText().toString(),
                    Integer.parseInt(mEdtEdad.getText().toString()),
                    Double.parseDouble(mEdtSueldo.getText().toString())
            );

            mEmpleados.add(empleado);
            vaciarCampos();
            Snackbar.make(mLayout, "Empleado añadido", Snackbar.LENGTH_SHORT).show();
        }
    }

    private void mostrarDatos() {
        double edadMedia = 0.0;
        double minSueldo = mEmpleados.get(0).getmSueldo();
        double maxSueldo = 0.0;
        int sumEdad = 0;
        int contador = 0;

        for (int i = 0; i < mEmpleados.size(); i++) {
            sumEdad += mEmpleados.get(i).getmEdad();
            contador++;

            if (mEmpleados.get(i).getmSueldo() < minSueldo)
                minSueldo = mEmpleados.get(i).getmSueldo();

            if (mEmpleados.get(i).getmSueldo() > maxSueldo)
                maxSueldo = mEmpleados.get(i).getmSueldo();
        }

        edadMedia = sumEdad / contador;

        mTxvDatos.setText("Edad media: " + edadMedia + "\n" +
            "Sueldo mínimo: " + minSueldo + "\n" +
            "Sueldo máximo: " + maxSueldo);
    }

    private void crearXML() {
        try {
            Analisis.crearXML(mEmpleados, NOMBRE_FICHERO);
            Snackbar.make(mLayout, "XML creado con éxito", Snackbar.LENGTH_SHORT).show();
        } catch (IOException e) {
            Snackbar.make(mLayout, "Error al crear XML", Snackbar.LENGTH_SHORT).show();
        }
    }
}
